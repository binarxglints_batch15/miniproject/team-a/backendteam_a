const path = require("path");
const crypto = require("crypto");
const cloudinary = require("cloudinary").v2;
const validator = require("validator");
const { promisify } = require("util");

exports.registerAndUpdateValidator = async (req, res, next) => {
  try {
    const errors = [];
    const fullname = req.body.first_name + req.body.last_name
    
    if (validator.isEmpty(req.body.first_name, { ignore_whitespace:false })) {
      errors.push("Please enter your first name");
    }

    if (validator.isEmpty(req.body.last_name, { ignore_whitespace:false })) {
      errors.push("Please enter your last name");
    }

    if (fullname.length < 5) {
      errors.push("Fullname minimum 5 characters");
    }

    if (!validator.isEmail(req.body.email)) {
        errors.push("Please enter the correct Email address");
    }

    if (!validator.isStrongPassword(req.body.password,[{ minLength: 10, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1,}])) {
      errors.push("Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters");
    }

    if (!validator.isStrongPassword(req.body.confirmPassword,[{ minLength: 10, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1,}])) {
      errors.push("Confirmation Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters");
    }

    if (!validator.equals(req.body.password, req.body.confirmPassword)) {
        errors.push("Password and Password Confirmation is not match");
    }

    if (errors.length > 0) {
      return res.status(400).json({ success: false, errors: errors });
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, errors: ["Internal Server Error"] });
  }
};