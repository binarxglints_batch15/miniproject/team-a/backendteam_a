const validator = require("validator");

exports.reviewValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (!req.body.comment) {
      errors.push("Please enter your comment");
    }

    if (!validator.isLength(req.body.comment, {min: 0, max:255})) {
      errors.push("Maximum characters is 255");
    }

    if (errors.length > 0) {
      return res.status(400).json({ success: false, errors: errors });
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, errors: ["Internal Server Error"] });
  }
};