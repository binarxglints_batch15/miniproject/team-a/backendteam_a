const path = require("path");
const crypto = require("crypto");
const cloudinary = require("cloudinary").v2;
const validator = require("validator");
const { promisify } = require("util");

exports.eventValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (validator.isEmpty(req.body.id_category, { ignore_whitespace:false })) {
      errors.push("Please enter the event category");
    }
    if (validator.isEmpty(req.body.event_title, { ignore_whitespace:false })) {
      errors.push("Please enter the event title");
    }
    if (validator.isEmpty(req.body.event_detail, { ignore_whitespace:false })) {
      errors.push("Please enter the event detail");
    }
    if (!validator.isDate(req.body.date_and_time,)) {
      errors.push("Please enter the date using format (YYYY/MM/DD");
    }
 
    if (!req.body.date_and_time) {
      errors.push("Please insert event date and time");
    }

    if (errors.length > 0) {
      return res.status(400).json({ success: false, errors: errors });
    }

    if (req.files.event_photo) {
      const file = req.files.event_photo;

      if (!file.mimetype.startsWith("image/")) {
        errors.push("File must be an image");
      }

      if (file.size > 2000000) {
        errors.push("Image must be less than 2MB");
      }

      if (errors.length > 0) {
        return res.status(400).json({ success: false, errors: errors });
      }

      let fileName = crypto.randomBytes(16).toString("hex");

      file.name = `${fileName}${path.parse(file.name).ext}`;

      const move = promisify(file.mv);

      await move(`./public/images/${file.name}`);

      const image = await cloudinary.uploader.upload(`./public/images/${file.name}`)
      .then((result) => {
          return result.secure_url
      });

      req.body.event_photo = image;

    }

    if (req.body.event_photo == null) {
      errors.push("Please insert event image");
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, errors: ["Internal Server Error"] });
  }
};