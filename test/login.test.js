const request = require("supertest");
const app = require("../index");
const { encodePin } = require("../utils");
const { user } = require("../models");

beforeAll(async () => {
  const hashPassword = encodePin("GAcademy#15");
  let users = await user.create({
    first_name: "nanda",
    last_name: "nif",
    email: "nandanif@gmail.com",
    password: hashPassword,
  });
});
afterAll((done) => {
  user
    .destroy({ where: {}, force: true })
    .then(() => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

describe("User try to login:", () => {
  describe("Success:", () => {
    it("Should return 200 and access_token", (done) => {
      let input = {
        email: "nandanif@gmail.com",
        password: "GAcademy#15",
      };
      request(app)
        .post("/user/login")
        .send(input)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(body).toHaveProperty("success");
          expect(body).toHaveProperty("token");
          expect(body.success).toBe(true);
          expect(body.token).toBeTruthy();
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Failed:", () => {
    describe("Empty field", () => {
      it("Should return 400", (done) => {
        let input = {
          email: "",
          password: "",
        };
        request(app)
          .post("/user/login")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            expect(body).toHaveProperty("success");
            expect(body).toHaveProperty("errors");
            expect(body.success).toBe(false);
            expect(body.errors).toContain("Please enter the correct email address", "Please enter your password");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("Failed:", () => {
      describe("Empty password", () => {
        it("Should return 400", (done) => {
          let input = {
            email: "nandanif@gmail.com",
            password: "",
          };
          request(app)
            .post("/user/login")
            .send(input)
            .then((response) => {
              let { body, status } = response;
              expect(status).toBe(400);
              expect(body).toHaveProperty("success");
              expect(body).toHaveProperty("errors");
              expect(body.success).toBe(false);
              expect(body.errors).toContain("Please enter your password");
              done();
            })
            .catch((err) => {
              done(err);
            });
        });
      });
    });

    describe("Failed:", () => {
        describe("Empty email", () => {
          it("Should return 400", (done) => {
            let input = {
              email: "",
              password: "GAcademy#15",
            };
            request(app)
              .post("/user/login")
              .send(input)
              .then((response) => {
                let { body, status } = response;
                expect(status).toBe(400);
                expect(body).toHaveProperty("success");
                expect(body).toHaveProperty("errors");
                expect(body.success).toBe(false);
                expect(body.errors).toContain("Please enter the correct email address");
                done();
              })
              .catch((err) => {
                done(err);
              });
          });
        });
      });
  });
});