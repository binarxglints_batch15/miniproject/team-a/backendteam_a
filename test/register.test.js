const request = require("supertest");
const app = require("../index");
const { user } = require("../models");

beforeAll(async () => {

});
afterAll((done) => {
  user
    .destroy({ where: {}, force: true })
    .then(() => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

describe("User try to register:", () => {
  describe("Success:", () => {
    it("Should return 200", (done) => {
      let input = {
        first_name: "nanda",
        last_name: "nif",
        email: "nandanif@gmail.com",
        password: "GAcademy#15",
        confirmPassword: "GAcademy#15"
      };
      request(app)
        .post("/user/register")
        .send(input)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(body).toHaveProperty("success");
          expect(body).toHaveProperty("data");
          expect(body.success).toBe(true);
          expect(body.data.first_name).toBe("nanda");
          expect(body.data.last_name).toBe("nif");
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Failed:", () => {
    describe("Empty field", () => {
      it("Should return 400", (done) => {
        let input = {
            first_name: "",
            last_name: "",
            email: "",
            password: "",
            confirmPassword: ""
        };
        request(app)
          .post("/user/register")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            expect(body).toHaveProperty("success");
            expect(body).toHaveProperty("errors");
            expect(body.success).toBe(false);
            expect(body.errors).toContain("Please enter your first name",
            "Please enter your last name",
            "Fullname minimum 5 characters",
            "Please enter the correct Email address",
            "Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters",
            "Confirmation Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("Failed:", () => {
      describe("Password not contain lowercase, uppercase, number and special character", () => {
        it("Should return 400", (done) => {
          let input = {
            first_name: "nanda",
            last_name: "nif",
            email: "nandanif@gmail.com",
            password: "password",
            confirmPassword: "password"
          };
          request(app)
            .post("/user/register")
            .send(input)
            .then((response) => {
              let { body, status } = response;
              expect(status).toBe(400);
              expect(body).toHaveProperty("success");
              expect(body).toHaveProperty("errors");
              expect(body.success).toBe(false);
              expect(body.errors).toContain("Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters",
              "Confirmation Password => Please fill in the password with a length of 10-20 characters, consisting of a combination of uppercase letters, lowercase letters, numbers, and special characters");
              done();
            })
            .catch((err) => {
              done(err);
            });
        });
      });
    });

    describe("Failed:", () => {
        describe("Password and Confirm Password not match", () => {
          it("Should return 400", (done) => {
            let input = {
                first_name: "nanda",
                last_name: "nif",
                email: "nandanif@gmail.com",
                password: "GAcademy#15",
                confirmPassword: "GAcademy16"
            };
            request(app)
              .post("/user/register")
              .send(input)
              .then((response) => {
                let { body, status } = response;
                expect(status).toBe(400);
                expect(body).toHaveProperty("success");
                expect(body).toHaveProperty("errors");
                expect(body.success).toBe(false);
                expect(body.errors).toContain("Password and Password Confirmation is not match");
                done();
              })
              .catch((err) => {
                done(err);
              });
          });
        });
      });
  });
});