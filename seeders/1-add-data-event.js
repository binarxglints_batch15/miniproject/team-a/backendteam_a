'use strict';
const faker = require('faker');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('events', [
      {
        id_category: 5,
        id_user: 14,
        event_title: faker.lorem.sentence(),
        event_photo: faker.image.business(),
        event_detail: faker.lorem.paragraphs(),
        date_and_time: faker.date.future(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_category: 5,
        id_user: 11,
        event_title: faker.lorem.sentence(),
        event_photo: faker.image.business(),
        event_detail: faker.lorem.paragraphs(),
        date_and_time: faker.date.future(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_category: 6,
        id_user: 13,
        event_title: faker.lorem.sentence(),
        event_photo: faker.image.business(),
        event_detail: faker.lorem.paragraphs(),
        date_and_time: faker.date.future(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_category: 6,
        id_user: 13,
        event_title: faker.lorem.sentence(),
        event_photo: faker.image.business(),
        event_detail: faker.lorem.paragraphs(),
        date_and_time: faker.date.future(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_category: 4,
        id_user: 11,
        event_title: faker.lorem.sentence(),
        event_photo: faker.image.business(),
        event_detail: faker.lorem.paragraphs(),
        date_and_time: faker.date.future(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('events', null, {});
  }
};
