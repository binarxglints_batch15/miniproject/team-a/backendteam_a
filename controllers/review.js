const { review, event, user } = require("../models");

class Review {
  async getAllReview(req, res, next) {
    try {

      const { id_event } = req.query

      const data = await review.findAll({
        where: {
          id_event: +id_event
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: user,
            attributes: ["first_name"]
          },
          {
            model: user,
            attributes: ["last_name"]
          },
          {
            model: user,
            attributes: ["email"]
          },
          {
            model: event,
            attributes: ['event_title']
          }
        ],
        order: [['id', 'DESC']],
      });

       if (data.length === 0) {
         return res.status(400).json({ success: false, errors: ["Review not found!"] });
       }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }
  async getDetailReview(req, res, next) {
    try {
      const { id_event, id_review } = req.query

      const data = await review.findOne({
        where: { 
          id_event: +id_event,
          id: +id_review,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: user,
            attributes: ["first_name"]
          },
          {
            model: user,
            attributes: ["last_name"]
          },
          {
            model: user,
            attributes: ["email"]
          },
          {
            model: event,
            attributes: ['event_title']
          }
        ],
      });

      if (data == null) {
        return res.status(404).json({ success: false, errors: ["Review not found"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async createReview(req, res, next) {
    try {
      const userId = req.userData.id
      const { id_event } = req.query

      const newData = await review.create({
        id_user: +userId,
        id_event: +id_event,
        comment: req.body.comment
      });

      res.status(201).json({ success: true, message: ["Success adding review"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async updateReview(req, res, next) {
    try {

      const userId = req.userData.id
      const { id_event, id_review } = req.query;

      const checkUser = await review.findOne({
        where: {
            id: +id_review
        }
      })

      if(checkUser.dataValues.id_user != userId) {
        return res.status(401).json({
        success: false,
        message: "Access Denied"
        })
      }

      const updateData = await review.update({comment: req.body.comment},
        {
        where: { 
          id: +id_review,
        },
      });

      if (updateData == null) {
        return res.status(404).json({ success: false, errors: ["Review not found or has been updated"] });
      }

      res.status(201).json({ success: true, message: ["Success updating review"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async deleteReview(req, res, next) {
    try {

      const userId = req.userData.id
      const { id_review } = req.query;

      const checkUser = await review.findOne({
        where: {
            id: +id_review
        }
      })

      if(checkUser.dataValues.id_user != userId) {
        return res.status(401).json({
        success: false,
        message: "Access Denied"
        })
      }

      const deletedData = await review.destroy({
        where: {
          id: +id_review,
        },
        force: true
      });

      res.status(200).json({ success: true, message: ["Success deleting data"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Review();
