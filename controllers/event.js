const { event, category, user } = require("../models");
const { Op } = require('sequelize')
const { today } = require('../utils/index')

class Event {
  async getAllEventFiltered(req, res, next) {
    try {

      let { 
        page = 1,
        limit = 8,
        cat = 1,
        startDt = '1900-12-12',
        endDt = '2200-12-12',
        orders = 'id',
        sort = 'DESC'
      } = req.query
      

      const data = await event.findAll({
        where: {
          date_and_time: {[Op.between]: [startDt, endDt]},
          id_category: +cat
        },
        attributes: {
          exclude: ["link_conference","speaker_photo","speaker_name","createdAt","updatedAt","deletedAt",],
        },
        include: [
          {
            model: category,
            attributes: ["name"],
          },
          {
            model: user,
            attributes: ["email"],
          }
        ],
        order: [[orders || 'createdAt', sort || 'DESC']],
        limit: +limit,
        offset: ( +page - 1 ) * parseInt(limit)
      });

      if(data.length == 0) {
        res.status(500).json({ success: false, errors: ["Events not found"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async getAllEvent(req, res, next) {
    try {

      const data = await event.findAll({
        attributes: {
          exclude: ["link_conference","speaker_photo","speaker_name","createdAt","updatedAt","deletedAt",],
        },
        include: [
          {
            model: category,
            attributes: ["name"],
          },
          {
            model: user,
            attributes: ["first_name"]
          },
          {
            model: user,
            attributes: ["last_name"]
          },
          {
            model: user,
            attributes: ["email"]
          },
        ],
        order: [['createdAt', 'DESC']],
      });

      if(data.length == 0) {
        res.status(500).json({ success: false, errors: ["Events not found"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async getDetailEvent(req, res, next) {
    try {
      const { idEvent } = req.params;

      const data = await event.findOne({
        where: { id: idEvent },
        attributes: {
          exclude: ["link_conference","speaker_photo","speaker_name","createdAt","updatedAt","deletedAt",],
        },
        include: [
          {
            model: category,
            attributes: ["name"],
          },
          {
            model: user,
            attributes: ["first_name"]
          },
          {
            model: user,
            attributes: ["last_name"]
          },
          {
            model: user,
            attributes: ["email"]
          },
        ],
      });

      if (data == null) {
        return res.status(404).json({ success: false, errors: ["Event is not found"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async createEvent(req, res, next) {
    try {

      const userId = req.userData.id
      const {id_category, event_title, event_photo, event_detail, date_and_time, link_conference, speaker_photo, speaker_name} = req.body
      const data = await event.create({
        id_user: +userId,
        id_category,
        event_title,
        event_photo,
        event_detail,
        date_and_time,
        link_conference,
        speaker_photo,
        speaker_name
      });

      res.status(201).json({ success: true, message: ['Congratulations!!! Success create new event'] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async searchEvent(req, res, next) {
    try {
        const { cat, keyword } = req.query
        const data = await event.findAll({
          where: {
            id_category: +cat,
            event_title: {
              [Op.like]: `%${keyword}%`
            }
          },
          attributes: {
            exclude: ["link_conference","speaker_photo","speaker_name","createdAt","updatedAt","deletedAt",],
          },
          include: [
            {
              model: category,
              attributes: ["name"],
            },
            {
              model: user,
              attributes: ["email"],
            }
          ],
        });

        if (data.length == 0) {
          return res.status(404).json({ success: false, errors: ["Event is not found"] });
        }

      res.status(200).json({ success: true, data: data });     
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async searchHome(req, res, next) {
    try {
        const { search } = req.query
        const data = await event.findAll({
          where: {
            event_title: {
              [Op.like]: `%${search}%`
            }
          },
          attributes: {
            exclude: ["link_conference","speaker_photo","speaker_name","createdAt","updatedAt","deletedAt",],
          },
          include: [
            {
              model: category,
              attributes: ["name"],
            },
            {
              model: user,
              attributes: ["email"],
            }
          ],
        });

        if (data.length == 0) {
          return res.status(404).json({ success: false, errors: ["Event is not found"] });
        }

      res.status(200).json({ success: true, data: data });     
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async updateEvent(req, res, next) {
    try {

      const userId = req.userData.id
      const {id_category, event_title, event_photo, event_detail, date_and_time, link_conference, speaker_photo, speaker_name} = req.body
      const { idEvent } = req.params;
 
      const checkUser = await event.findOne({
        where: {
            id: +idEvent
        }
      })

      if(checkUser.dataValues.id_user != userId) {
          return res.status(401).json({
          success: false,
          message: "Access Denied"
          })
      }

      const updateData = await event.update({
        id_category,
        event_title,
        event_photo,
        event_detail,
        date_and_time,
        link_conference,
        speaker_photo,
        speaker_name
      }, {
        where: { id: idEvent },
      });

      const data = await event.findOne({
        where: {
          id: idEvent,
        },
        attributes: {
          exclude: ["link_conference", "speaker_photo","speaker_name", "createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: category,
            attributes: ['name']
          },
          {
            model: user,
            attributes: ["first_name"]
          },
          {
            model: user,
            attributes: ["last_name"]
          },
          {
            model: user,
            attributes: ["email"]
          },
        ],
      });

      res.status(201).json({ success: true, data: data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async deleteEvent(req, res, next) {
    try {

      const userId = req.userData.id
      const { idEvent } = req.params;

      const checkUser = await event.findOne({
        where: {
            id: idEvent
        }
      })

      if(checkUser.dataValues.id_user != userId) {
        return res.status(401).json({
        success: false,
        message: "Access Denied"
        })
      }

      const deletedData = await event.destroy({
        where: {
          id: idEvent,
        },
        force: true
      });

      res.status(200).json({ success: true, message: ["Event has been deleted"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Event();
