'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.event.hasMany(models.review, { foreignKey: "id_event" });
      models.event.hasMany(models.bookmark, { foreignKey: "id_event" });
      models.event.belongsTo(models.category, { foreignKey: "id_category" });
      models.event.belongsTo(models.user, { foreignKey: "id_user" });
    }
  };
  event.init({
    id_category: DataTypes.INTEGER,
    id_user: DataTypes.INTEGER,
    event_title: DataTypes.STRING,
    event_photo: DataTypes.STRING,
    event_detail: DataTypes.STRING,
    date_and_time: DataTypes.DATE,
    link_conference: DataTypes.STRING,
    speaker_photo: DataTypes.STRING,
    speaker_name: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'event',
  });
  return event;
};