'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
   
    static associate(models) {
      models.user.hasMany(models.event, {
        foreignKey: "id_user"
      });
      models.user.hasMany(models.review, {
        foreignKey: "id_user"
      });
      models.user.hasMany(models.bookmark, {
        foreignKey: "id_user"
      });
    }
  };
  user.init({
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "user",
    }
  );
  
  return user;
};