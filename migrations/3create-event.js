'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('events', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_category: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      id_user: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      event_title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      event_photo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      event_detail: {
        allowNull: false,
        type: Sequelize.STRING
      },
      date_and_time: {
        allowNull: false,
        type: Sequelize.DATE
      },
      link_conference: {
        allowNull: true,
        type: Sequelize.STRING
      },
      speaker_photo: {
        allowNull: true,
        type: Sequelize.STRING
      },
      speaker_name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
    // id_category foreign key
    await queryInterface.addConstraint('events', {
      fields: ['id_category'],
      type: 'foreign key',
      name: 'custom_fkey_id_category',
      references: {
        //Required field
        table: 'categories',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    // id_user foreign key
    await queryInterface.addConstraint('events', {
      fields: ['id_user'],
      type: 'foreign key',
      name: 'custom_fkey_id_user',
      references: {
        //Required field
        table: 'users',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('events');
  }
};