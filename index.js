require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });
const express = require("express");
const fileUpload = require("express-fileupload");
const cloudinary = require("cloudinary").v2;
const cors = require("cors")

const app = express();

cloudinary.config({ 
  cloud_name: process.env.CLOUD_NAME, 
  api_key: process.env.API_KEYCLOUD, 
  api_secret: process.env.API_SECRET
});

const corsOptions = {
    origin: '*',
    allowedHeaders: ['Content-Type', 'Access-Control-Request-Method', 'Access-Control-Request-Headers', 'Access-Control-Allow-Headers', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
    optionsSuccessStatus: 200
};

app.use(cors());


const router = require("./routes/index");
const errorHandler = require('./middlewares/errorHandler/errorHandler');

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());
app.use(express.static('public'));

app.use("/", router);

app.get('*', (req, res, next) => {
  res.send("404 Page Not Found");
});

app.use(errorHandler);

app.listen(port, () => console.log(`Server running smoothly on ${port}`));

module.exports = app;
