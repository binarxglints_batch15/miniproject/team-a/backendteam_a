const express = require('express');
const { registerAndUpdateValidator } = require('../middlewares/validators/registerAndUpdateValidator');
const { loginValidator } = require('../middlewares/validators/loginValidator');
const { eventValidator } = require('../middlewares/validators/eventValidator');
const { reviewValidator } = require('../middlewares/validators/reviewValidator');
const { authentication } = require('../middlewares/auth/authentication');

const {
    createUser,
    getAllUser,
    getDetailUser,
    updateUser,
    deleteUser,
    login
} = require('../controllers/user');

const {
    createEvent,
    getAllEventFiltered,
    getAllEvent,
    getDetailEvent,
    searchEvent,
    searchHome,
    updateEvent,
    deleteEvent
} = require('../controllers/event');

const { 
    getAllBookmark, 
    addBookmark, 
    deleteBookmark 
} = require('../controllers/bookmark');

const{ 
    getAllReview,
    getDetailReview,
    createReview,
    updateReview,
    deleteReview 
} = require('../controllers/review');

const router = express.Router();

router.post('/user/register', registerAndUpdateValidator, createUser);
router.post('/user/login', loginValidator, login);

router.get('/users', authentication, getAllUser);
router.get('/user', authentication, getDetailUser);
router.put('/user', registerAndUpdateValidator, authentication, updateUser);
router.delete('/user', authentication, deleteUser);

router.get('/search', searchEvent);
router.get('/home', searchHome);
router.get('/events', getAllEventFiltered);
router.get('/event', getAllEvent);
router.get('/event/:idEvent', authentication, getDetailEvent);
router.post('/event', eventValidator, authentication, createEvent);
router.put('/event/:idEvent', eventValidator, authentication, updateEvent);
router.delete('/event/:idEvent', authentication, deleteEvent);

router.get('/bookmark', authentication, getAllBookmark);
router.post('/bookmark/:idEvent', authentication, addBookmark);
router.delete('/bookmark/:idBookmark', authentication, deleteBookmark);

router.get('/reviews', authentication, getAllReview);
router.get('/review', authentication, getDetailReview);
router.post('/review', reviewValidator, authentication, createReview);
router.put('/review', authentication, updateReview);
router.delete('/review', authentication, deleteReview);

module.exports = router; 